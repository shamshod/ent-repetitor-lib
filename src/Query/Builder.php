<?php

namespace EntRepetitorLib\Query;

class Builder extends \Illuminate\Database\Query\Builder
{

    public function whereNullOrZero($column)
    {
        return $this->where(function($query) use ($column) {
            $query->whereNull($column)->orWhere($column, 0);
        });
    }
}