<?php

namespace EntRepetitorLib\Eloquent;

class Collection extends \Illuminate\Database\Eloquent\Collection
{
    /**
     * Dynamic append attributes
     *
     * @custom
     * @param  array|string  $attributes
     * @return $this
     */
    public function append($attributes)
    {
        $this->each(function ($model) use ($attributes) {
            $model->append($attributes);
        });
        return $this;
    }
}