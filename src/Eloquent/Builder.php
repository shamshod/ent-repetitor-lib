<?php

namespace EntRepetitorLib\Eloquent;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class Builder extends \Illuminate\Database\Eloquent\Builder
{
    /**
     * Prevent the specified relations from being eager loaded.
     *
     * @param  mixed  $relations
     * @return $this
     */
    public function without($relations)
    {
        if (is_string($relations)) {
            $relations = func_get_args();
        }
        $this->eagerLoad = array_diff_key($this->eagerLoad, array_flip($relations));
        return $this;
    }

    /**
     * Paginate the given query.
     *
     * @param  int  $perPage
     * @param  array  $columns
     * @param @custom $append – append attributes
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function paginate($perPage = null, $append = [], $columns = ['*'], $pageName = 'page', $page = null)
    {
        $query = $this->toBase();
        $total = $query->getCountForPagination();
        $this->forPage(
            $page = $page ?: Paginator::resolveCurrentPage($pageName),
            $perPage = $perPage ?: $this->model->getPerPage()
        );
        return new LengthAwarePaginator($this->get($columns)->append($append), $total, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);
    }
}
