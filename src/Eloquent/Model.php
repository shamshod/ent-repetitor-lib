<?php

namespace EntRepetitorLib\Eloquent;

use \App\Events\LogAction;
use EntRepetitorLib\Query\Builder as QueryBuilder;
use EntRepetitorLib\Eloquent\Builder as EloquentBuilder;
use EntRepetitorLib\Eloquent\Collection;
use Carbon\Carbon;

class Model extends \Illuminate\Database\Eloquent\Model
{
    /**
     * Model should be loggable
     *
     * @var boolean
     * @custom
     */
    protected $loggable = true;

    /**
     * Comma-separated attributes
     *
     */
    protected $commaSeparated = [];

    /**
     * Dot dates attributes (15.05.1992)
     *
     * @custom
     * @var array
     */
    protected $dotDates = [];
    /**
     * Virtual attributes that aren't saved to database
     *
     * @custom
     * @var array
     */
    protected $virtual = [];

    /**
     * Indicates if the model was deleted during the current request lifecycle.
     *
     * @var bool
     * @custom
     */
    public $wasRecentlyDeleted = false;

    /**
     * Get a plain attribute (not a relationship).
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttributeValue($key)
    {
        $value = parent::getAttributeValue($key);

        if (in_array($key, $this->commaSeparated)) {
            return $this->getCommaSeparated($value);
        }

        if (in_array($key, $this->dotDates)) {
            return static::getDotDate($value);
        }

        return $value;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->commaSeparated)) {
            $value = $this->setCommaSeparated($value);
        }
        if (in_array($key, $this->dotDates)) {
            $value = $this->setDotDate($value);
        }

        return parent::setAttribute($key, $value);
    }

    /**
     * Convert the model's attributes to an array.
     *
     * @return array
     */
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();

        foreach ($this->commaSeparated as $key) {
            if (! array_key_exists($key, $attributes)) {
                continue;
            }
            $attributes[$key] = $this->getCommaSeparated($attributes[$key]);
        }

        foreach ($this->dotDates as $key) {
            if (! array_key_exists($key, $attributes)) {
                continue;
            }
            $attributes[$key] = static::getDotDate($attributes[$key]);
        }
        return $attributes;
    }

    /**
     * Get comma-separated attribute
     */
    private function getCommaSeparated($value)
    {
        if (is_array($value)) {
            return $value;
        } else {
            if (empty($value)) {
                return [];
            } else {
                $values = explode(',', $value);
                // intval не нужно применять, если хранятся строки (test1,test2,test3)
                if (is_numeric($values[0])) {
                    $values = array_map('intval', $values);
                }
                return $values;
            }
        }
    }

    /**
     * Set comma-separated attribute
     */
    private function setCommaSeparated($value)
    {
        if (is_array($value)) {
            return implode(',', $value);
        }

        return $value;
    }

    /**
     * Get a clean attribute, avoiding accessors/getters
     *
     */
    public function getClean($key)
    {
        return $this->getAttributes()[$key];
    }

    /**
     * Check if one of the attributes changed
     * @ attributes – mixed – string | array
     */
    public function changed($attributes)
    {
        $changed = false;
        if (! is_array($attributes)) {
            $attributes = [$attributes];
        }
        foreach ($attributes as $attribute) {
            if (array_key_exists($attribute, $this->getDirty())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get dot date
     * @custom
     */
    private static function getDotDate($value)
    {
        if ($value) {
            return date('d.m.Y', strtotime($value));
        } else {
            return $value;
        }
    }
    /**
     * Set dot date
     * @custom
     */
    private static function setDotDate($value)
    {
        if (preg_match('/[\d]{2}[\.][\d]{2}[\.][\d]{4}/', $value)) {
            return Carbon::createFromFormat('d.m.Y', $value)->toDateString();
        } else {
            return $value;
        }
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        if (is_null($this->getKeyName())) {
            throw new Exception('No primary key defined on model.');
        }
        if ($this->exists) {
            if ($this->fireModelEvent('deleting') === false) {
                return false;
            }
            // Here, we'll touch the owning models, verifying these timestamps get updated
            // for the models. This will allow any caching to get broken on the parents
            // by the timestamp. Then we will go ahead and delete the model instance.
            $this->touchOwners();
            $this->performDeleteOnModel();
            $this->exists = false;

            $this->wasRecentlyDeleted = true;
            event(new LogAction($this));

            // Once the model has been deleted, we will fire off the deleted event so that
            // the developers may hook into post-delete operations. We will then return
            // a boolean true as the delete is presumably successful on the database.
            $this->fireModelEvent('deleted', false);
            return true;
        }
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        $query = $this->newQueryWithoutScopes();
        // If the "saving" event returns false we'll bail out of the save and return
        // false, indicating that the save failed. This provides a chance for any
        // listeners to cancel save operations if validations fail or whatever.
        if ($this->fireModelEvent('saving') === false) {
            return false;
        }
        // Unset virtual attributes before save
        // @custom
        foreach ($this->virtual as $attribute) {
            unset($this->{$attribute});
        }
        // If the model already exists in the database we can just update our record
        // that is already in this database using the current IDs in this "where"
        // clause to only update this model. Otherwise, we'll just insert them.
        if ($this->exists) {
            $saved = $this->performUpdate($query, $options);
        }
        // If the model is brand new, we'll insert it into our database and set the
        // ID attribute on the model to the value of the newly inserted row's ID
        // which is typically an auto-increment value managed by the database.
        else {
            $saved = $this->performInsert($query, $options);
        }
        // @custom
        if ($this->loggable) {
            event(new LogAction($this));
        }
        if ($saved) {
            $this->finishSave($options);
        }
        return $saved;
    }

    /**
     * Hide the specific relationship in the model.
     *
     * @custom
     * @param  string  $relation
     * @return $this
     */
    public function hideRelation($relation)
    {
        unset($this->relations[$relation]);
        return $this;
    }

    /**
     * Get a new query builder instance for the connection.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();
        $grammar = $conn->getQueryGrammar();
        return new QueryBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new EloquentBuilder($query);
    }

    /**
     * Append attributes to query when building a query.
     *
     * @param  array|string  $attributes
     * @return $this
     */
    public function append($attributes)
    {
        if (is_string($attributes)) {
            $attributes = func_get_args();
        }
        $this->appends = array_unique(
            array_merge($this->appends, $attributes)
        );
        return $this;
    }

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new Collection($models);
    }
}
